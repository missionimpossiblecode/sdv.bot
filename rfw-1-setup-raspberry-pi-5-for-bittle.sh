# This "run-from-web" code can be downloaded and run with this one line script:

# wget https://gitlab.com/missionimpossiblecode/sdv.bot/-/raw/main/rfw-1-setup-raspberry-pi-5-for-bittle.sh -O /tmp/setup.sh ; bash /tmp/setup.sh [Optional_new_hostname]

until passwd
do
  echo "You seem to have made a mistake, please try again. (You must provide the current password first.)"
done

sudo apt-get update
if [[ -z "$(command -v python3)" ]] ; then sudo apt-get install -y python3; fi
if [[ -z "$(command -v git)" ]] ; then sudo apt-get install -y git; fi

if [[ -n "$1" ]] && [[ "$(hostname)" != "$1" ]]; then
  NEWHOSTNAME=$1
  sudo hostnamectl set-hostname $NEWHOSTNAME
  sudo sed -i "s/raspberrypi/$NEWHOSTNAME/g" /etc/hosts
fi

if [[ $(grep "^dtoverlay=miniuart-bt" /boot/firmware/config.txt -c) -lt 1 ]]; then
  echo '#===>>> Reassign BT UART to Mini Uart, but keep bt working' | sudo tee -a /boot/firmware/config.txt
  echo 'dtoverlay=miniuart-bt' | sudo tee -a /boot/firmware/config.txt
fi

if [[ $(grep "^dtparam=i2c_arm=off" /boot/firmware/config.txt -c) -lt 1 ]]; then
  echo '#===>>> Force i2c off for self-documentation of requirement' | sudo tee -a /boot/firmware/config.txt 
  echo 'dtparam=i2c_arm=off' | sudo tee -a /boot/firmware/config.txt
fi

if [[ $(grep "^dtparam=spi=off" /boot/firmware/config.txt -c) -lt 1 ]]; then
  echo '#===>>> Force spi off for self-documentation of requirement' | sudo tee -a /boot/firmware/config.txt 
  echo 'dtparam=spi=off' | sudo tee -a /boot/firmware/config.txt
fi

echo "#===>>> The next two updates are equivalent to configuring the serial port using raspi-config described here: https://bittle.petoi.com/4-configuration#4-4-1-config-raspberry-pi-serial-port"
if [[ $(grep "^enable_uart=1" /boot/firmware/config.txt -c) -lt 1 ]]; then
  echo "#===>>> Enable Serial hardware"
  echo 'enable_uart=1' | sudo tee -a /boot/firmware/config.txt
fi

if [[ $(grep "console=serial0,115200" /boot/firmware/cmdline.txt -c) -ge 1 ]]; then
  echo "#===>>> Disable console on serial"
  sudo sed -i "s/console=serial0,115200 //g" /boot/firmware/cmdline.txt
fi

if [[ $(grep "dtoverlay=w1-gpio" /boot/firmware/config.txt -c) -ge 1 ]]; then
  echo "#===>>> Removing 1-Wire Configuration as required in: https://bittle.petoi.com/4-configuration#4-4-1-config-raspberry-pi-serial-port"
  sudo sed -i "s/dtoverlay=w1-gpio//g" /boot/firmware/config.txt
fi

if [[ $(grep "cgroup_enable=cpuset" /boot/firmware/cmdline.txt -c) -lt 1 ]]; then
  echo "#===>>> Adding cgroup settings for k3s"
  sudo sed -i "s/rootwait/rootwait cgroup_enable=cpuset cgroup_enable=memory cgroup_memory=1/g" /boot/firmware/cmdline.txt
fi

echo "#===>>> Setting up serial ports"
sudo systemctl stop serial-getty@ttyS0.service
sudo systemctl disable serial-getty@ttyS0.service
echo 'KERNEL=="ttyS0", SYMLINK+="serial0" GROUP="tty" MODE="0660"' | sudo tee -a /etc/udev/rules.d/50-tty.rules
echo 'KERNEL=="ttyAMA0", SYMLINK+="serial1" GROUP="tty" MODE="0660"' | sudo tee -a /etc/udev/rules.d/50-tty.rules
sudo udevadm control --reload-rules && sudo udevadm trigger
sudo chgrp -h tty /dev/serial0
sudo chgrp -h tty /dev/serial1
sudo adduser $USER tty
sudo adduser $USER dialout
sudo chmod g+r /dev/ttyS0
sudo chmod g+r /dev/ttyAMA0

sudo apt-get -y upgrade

echo "Use the following command to test for success after the reboot 'python3 ardSerial.py kbalance' Bittle should perform the balance instinct which shifts into a standing position."
echo .
read -n 1 -s -r -p "Press any key to reboot..."

sudo reboot now