# sdv.bot

Attempts to get SOAFEE Software Defined Vehicle architecture working on the Petoi Bittle robot 

Tested on Raspberry 5 w/ 8GB. It appears 4GB would be enough. It wouldn't be surprising if these exact instructions work for a Raspberry Pi 4 w/ 4GB.

1. Solder a 2 x 5 connector to the ardruino board of the Petoi Bittle (not "Bittle X") as documented here: https://youtu.be/iGYNq-T1fZc
2. Using the [Raspberry Pi Imager](https://www.raspberrypi.com/software/), prepare an SD card for a Raspberry Pi 5 using the 64-bit Raspberry Pi OS Lite image (no desktop) and [configure it to connect to your wifi network in the imager utility](https://www.raspberrypi.com/documentation/computers/getting-started.html#raspberry-pi-imager).
3. Connect the Raspberry Pi 5 to the 2x5 connector and be sure the Pi is connecting to your network.
4. Power up the Bittle.
5. SSH into the Pi.
6. Run `wget https://gitlab.com/missionimpossiblecode/sdv.bot/-/raw/main/rfw-1-setup-raspberry-pi-5-for-bittle.sh -O /tmp/setup.sh ; bash /tmp/setup.sh` and follow the prompts.
7. After reboot, ssh in and run `wget https://gitlab.com/missionimpossiblecode/sdv.bot/-/raw/main/rfw-2-k3s-and-bittle-ctrl-pod.sh -O /tmp/setup2.sh ; bash /tmp/setup2.sh`
8. After the Bittle Control Pod is fully running, enter it by running `kubectl exec bittlectrl -it -- bash`
9. From within the Bittle Control Pod, issue the "Stand up" command with `python3 serialMaster/ardSerial.py kbalance`

The Bittle should perform the balance instinct and move into a standing position.