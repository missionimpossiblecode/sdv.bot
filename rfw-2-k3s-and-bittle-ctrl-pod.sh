# This "run-from-web" code can be downloaded and run with this one line script:

# wget https://gitlab.com/missionimpossiblecode/sdv.bot/-/raw/main/rfw-2-k3s-and-bittle-ctrl-pod.sh -O /tmp/setup2.sh ; bash /tmp/setup2.sh

curl -sfL https://get.k3s.io | sh -s - --write-kubeconfig-mode 644

wget https://gitlab.com/missionimpossiblecode/sdv.bot/-/raw/main/bittlecontrolpod.yml -O bittlecontrolpod.yml

kubectl apply -f bittlecontrolpod.yml

kubectl get pods

echo "Once the pod is ready, use these two commands to enter the pod and then issue a command to bittle"
echo "  kubectl exec bittlectrl -it -- bash"
echo "  python3 serialMaster/ardSerial.py kbalance"
echo ""
echo "The Bittle should perform the balance instinct and move into a standing position."